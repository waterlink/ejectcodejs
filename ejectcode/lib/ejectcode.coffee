path = require "path"
ncp = require("ncp").ncp
fs = require "fs"
exec = require("child_process").exec

# I made this because uglifyjs does not appear to be in my PATH, how to make this better?
uglifyjs = path.resolve(__dirname, "../node_modules/uglify-js/bin/uglifyjs")

exports.clone = (source, destination, callback) ->
	console.log "[ejectcode] Cloning..."
	time = Date.now()
	nonce = Math.random()
	cloneid = "#{time}-#{nonce}"
	ncp.limit = 16
	fs.mkdir destination, (err) =>
		# throw new Error "Unable to create dir #{destination}: #{err}" if err
		clonehub = path.join(destination, "clones")
		fs.mkdir clonehub, (err) =>
			# throw new Error "Unable to create dir #{clonehub}: #{err}" if err
			currentClone = path.join clonehub, cloneid
			ncp source, currentClone,
				filter: (name) =>
					res = yes
					res = no if name.match /\.git/
					console.log name, res
					return res
			, (err) =>
				throw new Error "Unable to clone dir #{source} to #{currentClone}: #{err}" if err
				current = path.join destination, "current"
				fs.unlink current, =>
					fs.symlink path.join("clones", cloneid), current, "dir", (err) =>
						throw new Error "Unable to symlink dir #{currentClone} to #{current}: #{err}" if err
						console.log "[ejectcode] Cloning...OK"
						callback() if callback

exports.bake = (source, destination, callback) ->
	current = path.join destination, "current"
	exec "cd #{current} && . ./rules.bake && $JS && $CSS && $HTML", (err, stdout) =>
		throw new Error "Unable to bake: #{err}" if err
		for line in stdout.split "\n"
			console.log "[ejectcode] #{line}"
		callback() if callback


exports.minify = (source, destination, callback) ->
	current = path.join destination, "current"
	wait = 0
	ok = no
	exec "cd #{current} && find . -name '*.js'", (err, stdout) =>
		throw new Error "Unable to list js files: #{err}" if err
		for line in stdout.split "\n" when line.match /\.js$/
			unless line.match /\.min\.js$/
				# check if file has shebang
				go = (line) =>
					wait++
					exec "cd #{current} && head -1 #{line}", (err, stdout) =>
						throw new Error "Unable to parse file #{line}: #{err}" if err
						shebang = no
						if stdout[0] is "#"
							shebang = stdout
						minline = line.replace /\.js$/, ".min.js"
						console.log "[ejectcode] #{line} > #{minline}"
						execline = "cd #{current} && #{uglifyjs} -mc < #{line} > #{minline}"
						if shebang
							console.verblog "shebang!"
							execline = "cd #{current} && echo '#{shebang}' > #{minline} && tail -n +2 #{line} | #{uglifyjs} -mc | #{uglifyjs} -mc  > #{minline}"
						exec execline, (err, stdout, stderr) =>
							if err
								# console.log "error in code:"
								# exec "tail -n +2 #{line}", (err2, stdout) =>
									# console.log stdout
									# throw new Error "Unable to minify file #{line}: #{err}\n#{stderr}" if err
								console.verblog "[ejectcode] error in #{line}: #{err}"
							wait--
							if ok and wait is 0
								console.log "[ejectcode] Minifying: OK"
								ok = no
								callback() if callback
				go line
		ok = yes


exports.errorify = (source, destination, callback, increment = 20) =>
	current = path.join destination, "current"
	exec "cd #{current} && find . -name '*.js'", (err, stdout) =>
		throw new Error "Unable to list js files: #{err}" if err
		for line in stdout.split "\n" when line.match /\.js$/
			unless line.match /\.min\.js$/
				fs.unlink path.join(current, line), =>
	exec "cd #{current} && find . -name '*.coffee'", (err, stdout) =>
		throw new Error "Unable to list coffee files: #{err}" if err
		for line in stdout.split "\n" when line.match /\.coffee$/
			unless line.match /\.min\.coffee$/
				go = (line) =>
					step = 0
					result = ""
					next = (step) =>
						lineStart = step * increment
						exec "cd #{current} && cat #{line} | tail -n +#{lineStart} | head -#{increment}", (err, stdout) =>
							throw new Error "Unable to read #{line} from #{lineStart} by +#{increment}: #{err}" if err
							if stdout.replace(/\n/g, "").replace(/\r/g, "") is ""
								fs.writeFile path.join(current, line), result, (err) =>
									throw new Error "Unable to write file #{line}: #{err}" if err
								return
							lines = stdout.split "\n"
							best = 0
							for iter in [0...lines.length]
								if lines[best].length < lines[iter].length
									best = iter
							lines[best] = "// hidden by ejectcode"
							result += lines.join "\n"
							next step + 1
					next 0
				go line
	callback() if callback


exports.all = (source, destination, callback) ->
	exports.clone source, destination, =>
		exports.bake source, destination, =>
			exports.minify source, destination, =>
				exports.errorify source, destination, =>
					callback() if callback

