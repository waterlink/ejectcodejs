argv = require "optimist"
path = require "path"

eject = require "./ejectcode"

argv = argv.usage "Clones your code, bakes it, minifies it, obfuscates it, errorifies it\nUsage: ejectcode [options] [clone|bake|minify|obfuscate|errorify|all]"
argv = argv.demand 1
argv = argv.alias "s", "source"
argv = argv.describe "s", "path to project to work with"
argv = argv.default "s", "."
argv = argv.alias "d", "destination"
argv = argv.describe "d", "path to work directory (where actual baking, minifying, etc, happens)"
source = argv.argv.s
projname = path.basename path.resolve source
argv = argv.default "d", path.join("#{source}", "..", "#{projname}_review")
destination = argv.argv.d
argv = argv.alias "h", "help"
argv = argv.describe "h", "shows this message"
argv = argv.describe "_", "action"
argv = argv.boolean "v"
argv = argv.alias "b", "verbose"
optimist = argv

argv = argv.argv

if argv.h
	optimist.showHelp()

global.verbose = argv.v
console.verblog = ->
	console.log.call console, arguments if verbose

action = argv._[0]
unless action in ["clone", "bake", "minify", "obfuscate", "errorify", "all"]
	throw new Error "action must be one of [clone|bake|minify|obfuscate|all]"

eject[action] source, destination
